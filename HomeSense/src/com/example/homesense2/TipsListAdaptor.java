package com.example.homesense2;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;


	public class TipsListAdaptor extends ArrayAdapter<String>{
	private final Activity context;
	private final String[] tipsStringArray;
	private final String[] timeDateStringArray;


		public TipsListAdaptor(Activity context,
		String[] tipsStringArray, String[] timeDateStringArray) {
		super(context, R.layout.tips_list_layout, tipsStringArray);
		this.context = context;
		this.tipsStringArray = tipsStringArray;
		this.timeDateStringArray = timeDateStringArray;
		}
	
		@Override
		public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.tips_list_layout, null, true);
		TextView tipLabel = (TextView) rowView.findViewById(R.id.tipLabel);
		TextView tipTimeDate = (TextView) rowView.findViewById(R.id.tipTimeDate);

	
		tipLabel.setText(tipsStringArray[position]);
		tipTimeDate.setText(timeDateStringArray[position]);
		
		return rowView;
		}

}