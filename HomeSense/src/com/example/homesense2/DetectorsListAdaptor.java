package com.example.homesense2;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

	public class DetectorsListAdaptor extends ArrayAdapter<String>{
	private final Activity context;
	private final String[] detectorName;
	private final Integer[] imageId;
	private final Integer[] statusImageId;


		public DetectorsListAdaptor(Activity context,
		String[] detectorName, Integer[] imageId,  Integer[] statusImageId) {
		super(context, R.layout.control_detectors_list_layout, detectorName);
		this.context = context;
		this.detectorName = detectorName;
		this.imageId = imageId;
		this.statusImageId = statusImageId;
		}
	
		@Override
		public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.control_detectors_list_layout, null, true);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		ImageView imageView2 = (ImageView) rowView.findViewById(R.id.detector_status_icon);
		txtTitle.setText(detectorName[position]);
		imageView.setImageResource(imageId[position]);
		imageView2.setImageResource(statusImageId[position]);
		return rowView;
		}

}