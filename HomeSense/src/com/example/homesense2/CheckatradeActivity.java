package com.example.homesense2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CheckatradeActivity extends Activity {
	
	String tradesmanType = null;
	String postCode = null;
	String tradesmanCode = null;
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.checkatrade_layout);

        Bundle extras = getIntent().getExtras();
     	if(extras !=null) {
        tradesmanType = extras.getString("TradesmanType");
        tradesmanType = tradesmanType.toLowerCase().toString();
        postCode = extras.getString("Postcode");
     	}
     	
        Log.i(tradesmanType, "tradesmanType");

     	//Convert tradesmanType into code for url
     	if (tradesmanType.equals("plumber"))
     	{
     		tradesmanCode = "&cat=151";
     	}
     	else if (tradesmanType.equals("electrician"))
     	{
     		tradesmanCode = "&cat=12";
     	}
     	else if (tradesmanType.equals("roofer"))
     	{
     		tradesmanCode = "&cat=1189";
     	}
     	else if (tradesmanType.equals("tiler"))
     	{
     		tradesmanCode = "&cat=1014";
     	}
     	else if (tradesmanType.equals("bathrooms"))
     	{
     		tradesmanCode = "&cat=1038";
     	}
     	else if (tradesmanType.equals("insulation"))
     	{
     		tradesmanCode = "&cat=876";
     	}
     	else if (tradesmanType.equals("alarms and security"))
     	{
     		tradesmanCode = "&cat=1021";
     	}
         	else {tradesmanCode = null;}
     	
        Log.i(tradesmanCode, "tradesmanCode");

     			
		WebView wv = (WebView) findViewById(R.id.webView1);
		//Set the web view to use the web view client class defined below so that app doesn't open the web page in a browser.
	    wv.setWebViewClient(webClient);
	    //wv.getSettings().setJavaScriptEnabled(true);
	    String url = "http://www.checkatrade.com/Search/default.aspx?location=" + postCode + tradesmanCode;
        Log.i(url, "url");
        wv.loadUrl(url);
    }
    
    //Implement a web view client class to override the url loading in an android browser  - so that it loads inside the application instead!
    //Also handles phone number and email urls taking the user to the default dialer or email app
    WebViewClient webClient = new WebViewClient()
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView  view, String  url)
        {
	        if (url.startsWith("tel:")) { 
	            Intent intent = new Intent(Intent.ACTION_DIAL,
	                    Uri.parse(url)); 
	            startActivity(intent); 
	        	}
	        else if (url.startsWith("mailto:")){
                MailTo mt = MailTo.parse(url);
                Intent i = newEmailIntent(CheckatradeActivity.this, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                startActivity(i);
                view.reload();
                return true;
            }
	        else if (url.startsWith("http:") || url.startsWith("https:")) {
	        	view.loadUrl(url);
	        }
    return true;
        }
    };
    
    //Method for handling an email address in the web view
    public static Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_CC, cc);
        intent.setType("message/rfc822");
        return intent;
  }
      
}