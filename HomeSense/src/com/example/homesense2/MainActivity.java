package com.example.homesense2;

import java.util.Locale;
import java.io.IOException;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.Paint.Align;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.TabHost;
import android.widget.SeekBar;


public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    /** 
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    static final String TAG = "HomeSense";
    
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    
    String SENDER_ID = "24885018388";
    TextView mDisplay;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs; 
    Context context;
    String regid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
        
//        super.onCreate(savedInstanceState);
//
//        setContentView(R.layout.main);
//        mDisplay = (TextView) findViewById(R.id.display);

        context = getApplicationContext();

        if (checkPlayServices()) {
        	gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);
            
            if (regid.isEmpty()) {
                registerInBackground();
            }
        }else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }
    
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("MainActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    
    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return ""; 
        }
        return registrationId;
    }
    
    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
    
    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    
    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
    	Log.i(TAG, "Starting Registration in Background.");
        new AsyncTask<Void,Void,String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    Log.i(TAG, "Calling register(SENDER_ID).");
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;
                    Log.i(TAG, msg);
                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    Log.i(TAG, "Send regid to backend.");
                    sendRegistrationIdToBackend(regid);

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    Log.i(TAG, "Store regid on phone.");
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //mDisplay.append(msg + "\n");
            }

        }.execute(null, null, null);	
        
    }
    
    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend(String regid) {
//    	new AsyncTask<Void,Void,String>() {
//            @Override
//            protected String doInBackground(Void... params) {
//                String msg = "";
                try {
                	HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost("http://homesenseapp.appspot.com/storeregid");
//                	HttpPost httpPost = new HttpPost("http://192.168.0.9:8888/storeregid");
                    httpPost.addHeader("regid", regid);

                    Log.i(TAG, "Just about to send http request to " + httpPost.getURI());
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    Log.i(TAG, "Received http response..");

                    Log.i(TAG, httpResponse.toString());
                } catch (IOException ex) {
                	Log.w(TAG, "Error :" + ex.getMessage());
//                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
//                return msg;
//            }
//
//            @Override
//            protected void onPostExecute(String msg) {
//                mDisplay.append(msg + "\n");
//            }
//
//        }.execute(null, null, null);	
    }
    
/**
 	* Stores the registration ID and app versionCode in the application's
 * {@code SharedPreferences}.
 *
 * @param context application's context.
 * @param regId registration ID
 */
    private void storeRegistrationId(Context context, String regId) {
    final SharedPreferences prefs = getGCMPreferences(context);
    int appVersion = getAppVersion(context);
    Log.i(TAG, "Saving regId on app version " + appVersion);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(PROPERTY_REG_ID, regId);
    editor.putInt(PROPERTY_APP_VERSION, appVersion);
    editor.commit();
}

 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

    	 MenuInflater inflater = getMenuInflater();
    	    inflater.inflate(R.menu.main, menu);
    	    return true;
        }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

    	 switch (item.getItemId()) {
         case R.id.alerts:
        	 Intent intent = new Intent(this, NotificationsActivity.class);
        	 intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
             startActivity(intent);
             return true;
         default:
             return super.onOptionsItemSelected(item);
     }
    }
    
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
        	
        	Fragment fragment = new EnergyUsageFragment();
        	
        	if (position == 0){
	            fragment = new EnergyUsageFragment();
	            Bundle args = new Bundle();
	            fragment.setArguments(args);
        	}
        	else if (position == 1){
        		 fragment = new MySensorsFragment();
                 Bundle args = new Bundle();
                 fragment.setArguments(args);
        	}
        	else if  (position == 2){
       		 fragment = new TradesmenFragment();
             Bundle args = new Bundle();
             fragment.setArguments(args);
    	}
			return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }


    public static class EnergyUsageFragment extends Fragment {
    	
    	private GraphicalView electricityChart;
    	//private GraphicalView averageElectricityChart;

    	private GraphicalView gasChart;
    	private GraphicalView waterChart;

        private XYMultipleSeriesDataset electricityDataset = new XYMultipleSeriesDataset();
        //private XYMultipleSeriesDataset averageElectricityDataset = new XYMultipleSeriesDataset();
        private XYMultipleSeriesDataset gasDataset = new XYMultipleSeriesDataset();
        private XYMultipleSeriesDataset waterDataset = new XYMultipleSeriesDataset();

        private XYMultipleSeriesRenderer electricityRenderer = new XYMultipleSeriesRenderer();
        //private XYMultipleSeriesRenderer averageElectricityRenderer = new XYMultipleSeriesRenderer();
        private XYMultipleSeriesRenderer gasRenderer = new XYMultipleSeriesRenderer();
        private XYMultipleSeriesRenderer waterRenderer = new XYMultipleSeriesRenderer();

        private XYSeries electricityCurrentSeries;
        //private XYSeries averageElectricityCurrentSeries;
        private XYSeries gasCurrentSeries;
        private XYSeries waterCurrentSeries;

        private XYSeriesRenderer electricityCurrentRenderer;
        //private XYSeriesRenderer averageElectricityCurrentRenderer;
        private XYSeriesRenderer gasCurrentRenderer;
        private XYSeriesRenderer waterCurrentRenderer;
       
   	
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.energy_usage, container, false);
            
            TabHost th = (TabHost) rootView.findViewById(android.R.id.tabhost);
            th.setup();
            TabSpec specs = th.newTabSpec("tag1");
            specs.setContent(R.id.tab1);
            specs.setIndicator("Electricity");
            th.addTab(specs);
            specs = th.newTabSpec("tag2");
            specs.setContent(R.id.tab2);
            specs.setIndicator("Gas");
            th.addTab(specs);
            specs = th.newTabSpec("tag3");
            specs.setContent(R.id.tab3);
            specs.setIndicator("Water");
            th.addTab(specs);
            
            for (int i = 0; i < th.getTabWidget().getChildCount(); i++) {
                TextView tv = (TextView) th.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                tv.setTextSize(10);
                tv.setMinHeight(14);
            }
            
            //Set up Spinners for changing between day, week, month, quarter and year
            Spinner electricityChartRangeSpinner = (Spinner)rootView.findViewById(R.id.spinnerElectricityChartRange);
            Spinner gasChartRangeSpinner = (Spinner)rootView.findViewById(R.id.spinnerGasChartRange);
            Spinner waterChartRangeSpinner = (Spinner)rootView.findViewById(R.id.spinnerWaterChartRange);
            ArrayAdapter<CharSequence> chartRangeAdapter = ArrayAdapter.createFromResource(getActivity().getBaseContext(), R.array.chart_range_array, android.R.layout.simple_spinner_item);
            electricityChartRangeSpinner.setAdapter(chartRangeAdapter);
            gasChartRangeSpinner.setAdapter(chartRangeAdapter);
            waterChartRangeSpinner.setAdapter(chartRangeAdapter);

            return rootView;
        }
        
        @Override
		public void onResume() {
            super.onResume();
            LinearLayout electricityChartLL = (LinearLayout) getView().findViewById(R.id.electricity_chart);
            if (electricityChart == null) {
                initElectricityChart();
                addElectricityData();
                electricityChart = ChartFactory.getBarChartView(getActivity(), electricityDataset, electricityRenderer, Type.DEFAULT);
                electricityChartLL.addView(electricityChart);
            }
            else {
            	electricityChart = ChartFactory.getBarChartView(getActivity(), electricityDataset, electricityRenderer, Type.DEFAULT);
            	electricityChartLL.addView(electricityChart);
                }
                
            LinearLayout gasChartLL = (LinearLayout) getView().findViewById(R.id.gas_chart);
                if (gasChart == null) {
                initGasChart();
                addGasData();
                gasChart = ChartFactory.getBarChartView(getActivity(), gasDataset, gasRenderer, Type.DEFAULT);
                gasChartLL.addView(gasChart);
		        }
		        else {
		        	gasChart = ChartFactory.getBarChartView(getActivity(), gasDataset, gasRenderer, Type.DEFAULT);
		        	gasChartLL.addView(gasChart);		        }
                
                LinearLayout waterChartLL = (LinearLayout) getView().findViewById(R.id.water_chart);
                if (waterChart == null) {
                initWaterChart();
                addWaterData();
                waterChart = ChartFactory.getBarChartView(getActivity(), waterDataset, waterRenderer, Type.DEFAULT);
                waterChartLL.addView(waterChart);
                }
                else {
                	waterChart = ChartFactory.getBarChartView(getActivity(), waterDataset, waterRenderer, Type.DEFAULT);
                	waterChartLL.addView(waterChart);}
        }
        
        //Initiate Graph methods
        private void initElectricityChart() {
            electricityCurrentSeries = new XYSeries("Electricity Usage");
            //averageElectricityCurrentSeries = new XYSeries("Average Electricity Usage");

            electricityDataset.addSeries(electricityCurrentSeries);
            //averageElectricityDataset.addSeries(averageElectricityCurrentSeries);

            electricityCurrentRenderer = new XYSeriesRenderer();
            //averageElectricityCurrentRenderer = new XYSeriesRenderer();

            electricityRenderer.setApplyBackgroundColor(true);
            //averageElectricityRenderer.setApplyBackgroundColor(true);

            electricityRenderer.setXTitle("Time");
            electricityRenderer.setYTitle("Kwh");
            electricityRenderer.setShowGrid(true);
            electricityRenderer.setBarSpacing(1);
            electricityRenderer.setBarWidth(20);
            electricityRenderer.setShowLegend(false);
            electricityRenderer.setPanEnabled(false, false);
            electricityRenderer.setZoomEnabled(false, false);
            electricityRenderer.setXLabels (0);
            
            electricityRenderer.setBackgroundColor(Color.WHITE);
            electricityRenderer.setMarginsColor(Color.WHITE);
            electricityRenderer.setAxesColor(Color.BLACK);
            electricityRenderer.setLabelsColor(Color.BLACK);
         
            electricityRenderer.setAxisTitleTextSize(20);
            electricityRenderer.setLegendTextSize(50);
            electricityRenderer.setChartTitleTextSize(20);
            electricityRenderer.setLabelsTextSize(15);
                
            electricityCurrentRenderer.setColor(Color.parseColor("#900099CC"));
            //averageElectricityCurrentRenderer.setColor(Color.parseColor("#900099CC"));
            
            //Add Time Values to x axis
            for (int i = 0; i < 25; i=i+4){
            	if (i < 10) {
            	electricityRenderer.addXTextLabel(i, "0"+i+":00");
            	}
            	else
            	{
            	electricityRenderer.addXTextLabel(i, i+":00");
                    	}
            }
            
            electricityRenderer.addSeriesRenderer(electricityCurrentRenderer);
            //mRenderer.setRange(chartRange);
        }
        
        private void initGasChart() {
        	//double[] chartRange = {0, 1000};
        	gasCurrentSeries = new XYSeries("Gas Usage");
            gasDataset.addSeries(gasCurrentSeries);
            gasCurrentRenderer = new XYSeriesRenderer();
            gasRenderer.setApplyBackgroundColor(true);
            gasRenderer.setBackgroundColor(Color.WHITE);
            gasRenderer.setMarginsColor(Color.WHITE);
            gasRenderer.setAxesColor(Color.BLACK);
            gasRenderer.setXTitle("Time");
            gasRenderer.setYTitle("Kwh");
            gasRenderer.setShowGrid(true);
            gasRenderer.setBarSpacing(1);
            gasRenderer.setBarWidth(20);
            gasRenderer.setShowLegend(false);
            gasRenderer.setLabelsColor(Color.BLACK);
            gasRenderer.setPanEnabled(false, false);
            gasRenderer.setZoomEnabled(false, false);
            gasRenderer.setXLabels (0);
            
            gasRenderer.setAxisTitleTextSize(20);
            gasRenderer.setLegendTextSize(50);
            gasRenderer.setChartTitleTextSize(20);
            gasRenderer.setLabelsTextSize(15);
            
            gasCurrentRenderer.setColor(Color.parseColor("#900099CC"));
            
            

            
            //Add Time Values to x axis
            for (int i = 0; i < 25; i=i+4){
            	if (i < 10) {
            		gasRenderer.addXTextLabel(i, "0"+i+":00");
            	}
            	else
            	{
            		gasRenderer.addXTextLabel(i, i+":00");
                    	}
            }
            
            gasRenderer.addSeriesRenderer(gasCurrentRenderer);
            //mRenderer.setRange(chartRange);

        }
        
        private void initWaterChart() {
        	//double[] chartRange = {0, 1000};
            waterCurrentSeries = new XYSeries("Water Usage");
            waterDataset.addSeries(waterCurrentSeries);
            waterCurrentRenderer = new XYSeriesRenderer();
            waterRenderer.setApplyBackgroundColor(true);
            waterRenderer.setBackgroundColor(Color.WHITE);
            waterRenderer.setMarginsColor(Color.WHITE);
            waterRenderer.setAxesColor(Color.BLACK);
            waterRenderer.setXTitle("Time");
            waterRenderer.setYTitle("Litres");
            waterRenderer.setShowGrid(true);
            waterRenderer.setBarSpacing(1);
            waterRenderer.setBarWidth(20);
            waterRenderer.setShowLegend(false);
            waterRenderer.setLabelsColor(Color.BLACK);
            waterRenderer.setFitLegend(true);
            waterRenderer.setPanEnabled(false, false);
            waterRenderer.setZoomEnabled(false, false);
            waterRenderer.setXLabels (0);
            waterCurrentRenderer.setColor(Color.parseColor("#900099CC"));
            
            waterRenderer.setAxisTitleTextSize(20);
            waterRenderer.setLegendTextSize(50);
            waterRenderer.setChartTitleTextSize(20);
            waterRenderer.setLabelsTextSize(15);

            
            //Add Time Values to x axis
            for (int i = 0; i < 25; i=i+4){
            	if (i < 10) {
            		waterRenderer.addXTextLabel(i, "0"+i+":00");
            	}
            	else
            	{
            		waterRenderer.addXTextLabel(i, i+":00");
                    	}
            }
            
            waterRenderer.addSeriesRenderer(waterCurrentRenderer);
            //mRenderer.setRange(chartRange);

        }
        
        //Add Data to graphs
        private void addElectricityData() {
              electricityCurrentSeries.add(0, 3);
              electricityCurrentSeries.add(2, 4);
              electricityCurrentSeries.add(4, 3);
              electricityCurrentSeries.add(6, 5);
              electricityCurrentSeries.add(8, 3);
              electricityCurrentSeries.add(10, 4);
              electricityCurrentSeries.add(12, 2);
              electricityCurrentSeries.add(14, 3);
              electricityCurrentSeries.add(16, 3);
              electricityCurrentSeries.add(18, 6);
              electricityCurrentSeries.add(20, 4);
              electricityCurrentSeries.add(22, 3);
              electricityCurrentSeries.add(24, 3);
              electricityCurrentSeries.addAnnotation("Washing Machine", 6, 4.5);
              electricityCurrentSeries.addAnnotation("Tumble Dryer", 18, 5.5);
              
              /*
              averageElectricityCurrentSeries.add(0, 3);
              averageElectricityCurrentSeries.add(2, 3);
              averageElectricityCurrentSeries.add(4, 3);
              averageElectricityCurrentSeries.add(6, 3);
              averageElectricityCurrentSeries.add(8, 3);
              averageElectricityCurrentSeries.add(10, 3);
              averageElectricityCurrentSeries.add(12, 3);
              averageElectricityCurrentSeries.add(14, 3);
              averageElectricityCurrentSeries.add(16, 3);
              averageElectricityCurrentSeries.add(18, 3);
              averageElectricityCurrentSeries.add(20, 3);
              averageElectricityCurrentSeries.add(22, 3);
              averageElectricityCurrentSeries.add(24, 3);
              */
        }

        private void addGasData() {
        	gasCurrentSeries.add(0, 50);
        	gasCurrentSeries.add(2, 40);
        	gasCurrentSeries.add(4, 40);
        	gasCurrentSeries.add(6, 40);
        	gasCurrentSeries.add(8, 50);
        	gasCurrentSeries.add(10, 40);
        	gasCurrentSeries.add(12, 45);
        	gasCurrentSeries.add(14, 40);
        	gasCurrentSeries.add(16, 35);
        	gasCurrentSeries.add(18, 40);
        	gasCurrentSeries.add(20, 35);
        	gasCurrentSeries.add(22, 45);
        	gasCurrentSeries.add(24, 35);
        	gasCurrentSeries.addAnnotation("Heating", 2, 40);
        	gasCurrentSeries.addAnnotation("Oven", 18, 40);
        }
        
        private void addWaterData() {
            waterCurrentSeries.add(0, 50);
            waterCurrentSeries.add(2, 40);
            waterCurrentSeries.add(4, 60);
            waterCurrentSeries.add(6, 45);
            waterCurrentSeries.add(8, 55);
            waterCurrentSeries.add(10, 50);
            waterCurrentSeries.add(12, 50);
            waterCurrentSeries.add(14, 55);
            waterCurrentSeries.add(16, 45);
            waterCurrentSeries.add(18, 55);
            waterCurrentSeries.add(20, 45);
            waterCurrentSeries.add(22, 40);
            waterCurrentSeries.add(24, 40);
            waterCurrentSeries.addAnnotation("Shower", 4, 55);
            waterCurrentSeries.addAnnotation("Tap", 18, 50);
        }

    }
    

    public static class MySensorsFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {
    	
    	//Values for Utilities List
    	  ListView utilitiesList;
          String[] utilitiesStringArray = {
        		  "Heating",
                  "Hot Water",
                  };
          Integer[] appliancesImageId = {
                  R.drawable.washing_machine,
                  R.drawable.tumble_dryer,
                  R.drawable.dish_washer,
                  };
          Integer[] seekBarControl = {
                 60,
                 40,
                 30,
                 };
    	    	
          // Values for Appliances List
    	  ListView appliancesList;
          String[] appliancesStringArray = {
	               	  "Washing Machine",
                      "Tumble Dryer",
                      "Dishwasher",
                   } ;
          
          SeekBar fridgeSeekbar = null;
          TextView fridgeSeekbarLabel = null;
          SeekBar freezerSeekbar = null;
          TextView freezerSeekbarLabel = null;
          
          //Values for Detectors List
    	  ListView detectorsList;
          String[] detectorsStringArray = {
                  "Dampness",
                  "Gas",
                  "Smoke",
                  "Motion",
                  "Glass-Break",
                   } ;
          Integer[]detectorsImageId = {
                  R.drawable.dampness,
                  R.drawable.gas,
                  R.drawable.smoke,
                  R.drawable.motion,
                  R.drawable.glass_break,
                  };
          
          	Integer[] statusImageId = {
                  R.drawable.red,
                  R.drawable.green,
                  R.drawable.green,
                  R.drawable.green,
                  R.drawable.green,
                 };

          SeekBar heatingSeekbar = null;
          TextView heatingSeekbarLabel = null;
          SeekBar hotWaterSeekbar = null;
          TextView hotWaterSeekbarLabel = null;
          
       @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.my_sensors, container, false);
            
            //Set up tab host
            TabHost th = (TabHost) rootView.findViewById(android.R.id.tabhost); 
            th.setup();
            TabSpec specs = th.newTabSpec("tag1");
            specs.setContent(R.id.tab1);
            specs.setIndicator("Utilities");
            th.addTab(specs);
            specs = th.newTabSpec("tag2");
            specs.setContent(R.id.tab2);
            specs.setIndicator("Appliances");
            th.addTab(specs);
            specs = th.newTabSpec("tag3");
            specs.setContent(R.id.tab3);
            specs.setIndicator("Detectors");
            th.addTab(specs);
            
            for (int i = 0; i < th.getTabWidget().getChildCount(); i++) {
                TextView tv = (TextView) th.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                tv.setTextSize(10);
                tv.setMinHeight(14);
            };
            
            /*Set up Utilities List adaptor        
            CustomList utilitiesAdapter = new CustomList(getActivity(), utilitiesStringArray, imageId, seekBarControl);
            utilitiesList=(ListView)rootView.findViewById(R.id.utilitiesList);
            utilitiesList.setAdapter(utilitiesAdapter); */
            
          //Set up Appliances List adaptor        
            CustomList appliancesAdapter = new CustomList(getActivity(), appliancesStringArray, appliancesImageId, seekBarControl);
            appliancesList=(ListView)rootView.findViewById(R.id.appliancesList);
            appliancesList.setAdapter(appliancesAdapter);
            
            //Set up Fridge and Freezer sliders
            fridgeSeekbar = (SeekBar) rootView.findViewById(R.id.fridgeSeekBar);
            fridgeSeekbarLabel = (TextView) rootView.findViewById(R.id.fridgeSeekbarLabel);
            fridgeSeekbar.setOnSeekBarChangeListener(this);
            
            freezerSeekbar = (SeekBar) rootView.findViewById(R.id.freezerSeekBar);
            freezerSeekbarLabel = (TextView) rootView.findViewById(R.id.freezerSeekbarLabel);
            freezerSeekbar.setOnSeekBarChangeListener(this);
            
          //Set up Detectors List adaptor         
            DetectorsListAdaptor detectorsAdapter = new DetectorsListAdaptor(getActivity(), detectorsStringArray, detectorsImageId, statusImageId);
            detectorsList=(ListView)rootView.findViewById(R.id.detectorsList);
            detectorsList.setAdapter(detectorsAdapter); 
            
            //Set up Heating and Hot Water sliders
            heatingSeekbar = (SeekBar) rootView.findViewById(R.id.heatingSeekBar);
            heatingSeekbarLabel = (TextView) rootView.findViewById(R.id.heatingSeekbarLabel);
            heatingSeekbar.setOnSeekBarChangeListener(this);
            
            hotWaterSeekbar = (SeekBar) rootView.findViewById(R.id.hotWaterSeekBar);
            hotWaterSeekbarLabel = (TextView) rootView.findViewById(R.id.hotWaterSeekbarLabel);
            hotWaterSeekbar.setOnSeekBarChangeListener(this);
            
            return rootView;
                                  
        }


	@Override
	public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
		switch (arg0.getId()) 
	    {
	    case R.id.heatingSeekBar:
	    	heatingSeekbarLabel.setText(": " + arg1 + "C");
	        break;	
	    case R.id.hotWaterSeekBar:
	    	hotWaterSeekbarLabel.setText(": " + arg1 + "C");
	        break;
	    case R.id.fridgeSeekBar:
	    	fridgeSeekbarLabel.setText(": " + arg1 + "C");
	        break;	
	    case R.id.freezerSeekBar:
	    	//Android does not support negative values in the seekbar so subtracting 30 from the value as a workaround
	    	freezerSeekbarLabel.setText(": " + (arg1-30) + "C");
	        break;
	    }
	}


	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStopTrackingTouch(SeekBar arg0) {
		// TODO Auto-generated method stub
		
	}     
  
    }
   
    //Resolve Fragment
    public static class TradesmenFragment extends Fragment implements OnClickListener {
    	     
	    String Postcode = null;
	    String tradesmanType = null;
	    Button btnSearchPostcode = null;
	    EditText postCodeField = null;
	    Spinner spinner = null;
	    Button btnNewHandymen = null;
	    
	 // Values for Favourite Handymen List
	  private ListView favouriteHandymenList;
	  private FavouriteHandymenDAO favouriteHandymenDAO;
	  
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.resolve, container, false);
            
            spinner = (Spinner)rootView.findViewById(R.id.tradesmenSpinner);
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getBaseContext(), R.array.tradesmen_types_array, android.R.layout.simple_spinner_item);
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);

            
            postCodeField = (EditText) rootView.findViewById(R.id.PostCode);
     	    Postcode = postCodeField.getText().toString();
            btnSearchPostcode=(Button)rootView.findViewById(R.id.btnSearchPostcode);
            btnSearchPostcode.setOnClickListener(this);
            
            
            
            TabHost th = (TabHost) rootView.findViewById(android.R.id.tabhost); 
            th.setup();
            TabSpec specs = th.newTabSpec("tag1");
            specs.setContent(R.id.tab1);
            specs.setIndicator("EDF Connect");
            th.addTab(specs);
            specs = th.newTabSpec("tag2");
            specs.setContent(R.id.tab2);
            specs.setIndicator("Tradesmen");
            th.addTab(specs);
            specs = th.newTabSpec("tag3");
            specs.setContent(R.id.tab3);
            specs.setIndicator("Favourites");
            th.addTab(specs);
            
            for (int i = 0; i < th.getTabWidget().getChildCount(); i++) {
                TextView tv = (TextView) th.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                tv.setTextSize(10);
                tv.setMinHeight(14);
            }
            
            favouriteHandymenList = (ListView) rootView.findViewById(R.id.favouriteHandymenList);
            favouriteHandymenDAO = new FileDAO(getActivity());
            ArrayAdapter<FavouriteHandymenDatabase> adapter1 = new ArrayAdapter<FavouriteHandymenDatabase>(getActivity(), R.layout.favourite_handymen_list, R.id.list_content, favouriteHandymenDAO.getAll());
            favouriteHandymenList.setAdapter(adapter1);
            
            final Intent j = new Intent(getActivity(), EnterFavouriteHandymenActivity.class);
            btnNewHandymen = (Button)rootView.findViewById(R.id.btnNewHandymen);
            
            btnNewHandymen.setOnClickListener(new OnClickListener() {
             public void onClick(View rootView) {
            	 startActivity(j);
        	
             }
       }); 
        
            return rootView;
        }
        
           
        //Pass tradesman type and postcode to the checkatrade activity on button press
        public void onClick(View rootView) {
     	    Postcode = postCodeField.getText().toString();
     	    tradesmanType = spinner.getSelectedItem().toString();
 	        Intent i = new Intent(getActivity(), CheckatradeActivity.class);
 	        i.putExtra("TradesmanType", tradesmanType);
 	        i.putExtra("Postcode", Postcode);
 	        startActivity(i);
 	    }
        
        public void onResume() {
        	super.onResume();
             favouriteHandymenDAO = new FileDAO(getActivity());
            ArrayAdapter<FavouriteHandymenDatabase> adapter1 = new ArrayAdapter<FavouriteHandymenDatabase>(getActivity(), R.layout.favourite_handymen_list, R.id.list_content, favouriteHandymenDAO.getAll());
            favouriteHandymenList.setAdapter(adapter1); 
 	    }

        
       
    }

}
