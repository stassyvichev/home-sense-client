package com.example.homesense2;

import java.util.ArrayList;
import java.io.Serializable;
// Interface where we add, remove and get all the data. (This part will may need to be modified so that instead of getting all the data we only get certain data.
public interface FavouriteHandymenDAO extends Serializable { 
	 
	public void add(FavouriteHandymenDatabase favouritehandymen);
	public void remove(FavouriteHandymenDatabase favouritehandymen); 
	public ArrayList<FavouriteHandymenDatabase> getAll();
	
}

