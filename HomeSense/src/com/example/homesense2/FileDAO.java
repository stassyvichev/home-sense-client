package com.example.homesense2;
// This class should be ignored. What this file does is basically what I said before by saving data in a local file on the android.
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException; 
import java.util.ArrayList;
import android.content.Context;


public class FileDAO implements FavouriteHandymenDAO { 
	
	private Context context; 
	private final String FILENAME = "favouritehandymencloud.dao";
		
	public FileDAO(Context context) {
		/*
		 * We need the activity context so we can find
		 * a valid location for writing our file.
		 */
		this.context = context;
	}
	
private ArrayList<FavouriteHandymenDatabase> createArrayList() {
		
		// check to see if the file already exists
		File file = new File(context.getFilesDir() + "/" +  FILENAME);
		if (!file.exists()) {
			return new ArrayList<FavouriteHandymenDatabase>();
		}
		
		// files exists -- read in the stored ArrayList<Note>
				ArrayList<FavouriteHandymenDatabase> allHandymen = null;
				try {
					FileInputStream fis = context.openFileInput(FILENAME);
					ObjectInputStream reader = new ObjectInputStream(fis);
					allHandymen = (ArrayList<FavouriteHandymenDatabase>) reader.readObject();
					reader.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (StreamCorruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
				
				return allHandymen;
}

private void saveArrayList(ArrayList<FavouriteHandymenDatabase> allHandymen) {
	ObjectOutputStream writer;
	try {
		FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
		writer = new ObjectOutputStream(fos);
		writer.writeObject(allHandymen);
		writer.close();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
}

public void add(FavouriteHandymenDatabase favouritehandymen) {
	ArrayList<FavouriteHandymenDatabase> allHandymen = createArrayList();
	allHandymen.add(favouritehandymen);
	saveArrayList(allHandymen);
}

public void remove(FavouriteHandymenDatabase favouritehandymen) {
	ArrayList<FavouriteHandymenDatabase> allHandymen = createArrayList();
	allHandymen.remove(favouritehandymen);
	saveArrayList(allHandymen);
}

/**
 * Return the list of notes.
 * 
 * @return The list of notes.
 */
public ArrayList<FavouriteHandymenDatabase> getAll() {
	return createArrayList();
}

}

