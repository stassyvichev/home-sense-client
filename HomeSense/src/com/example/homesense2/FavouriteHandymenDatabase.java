package com.example.homesense2;

import java.io.Serializable;
import java.text.SimpleDateFormat;
// This class is where we declare all the variables and state how the output will look like
public class FavouriteHandymenDatabase implements Serializable { 
	
	public String Name; 
	public String Occupation;
	public String Mobile;
	
	
	
	public FavouriteHandymenDatabase() {
	
	}

	public FavouriteHandymenDatabase(String Name, String Occupation, String Mobile) {
		this.Name = Name;
		this.Occupation = Occupation;
		this.Mobile = Mobile;
	}
	
	public FavouriteHandymenDatabase (FavouriteHandymenDatabase favouritehandymen){
		this.Name = new String(favouritehandymen.Name);
		this.Occupation = new String(favouritehandymen.Occupation);
		this.Mobile = new String (favouritehandymen.Mobile);
	}
	
	 public String getName() {
	        return Name;
	    }
	 
	 public String getOccupation() {
	        return Occupation;
	    }
	 
	 public String getMobile() {
	        return Mobile;
	    }
	
	 public void setMobile(String Mobile) {
	        this.Mobile = Mobile;
	    }
	 
	 public void setName(String Name) {
	        this.Name = Name;
	    }
	 
	 public void setOccupation(String Occupation) { 
	        this.Occupation = Occupation;
	    }
	 
	 @Override
	    public String toString() {
	        String newLine = "\n";
	        String s = "Name: " + getName() + newLine;
	        s += "Job: " + getOccupation() + newLine;
	        s += "Mobile: " + getMobile();
	        return s;
	    }
}

