package com.example.homesense2;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

	public class AppliancesListAdapter extends ArrayAdapter<String> //implements SeekBar.OnSeekBarChangeListener
	{
	private final Activity context;
	private final String[] web;
	private final Integer[] imageId;
	//private final Integer[] seekBarControl;
	
	//TextView seekBarLabel = null;
	//TextView seekBarTracking = null;

		public AppliancesListAdapter(Activity context,
		String[] web, Integer[] imageId) {
		super(context, R.layout.control_appliances_list_layout, web);
		this.context = context;
		this.web = web;
		this.imageId = imageId;
		//this.seekBarControl = seekBarControl;
		}
		
		/*public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
			seekBarLabel.setText("- " + progress + "C");
	    }
		
		public void onStartTrackingTouch(SeekBar seekBar) {
			seekBarTracking.setText(" ");
	    }
	 
	    public void onStopTrackingTouch(SeekBar seekBar) {
	    	seekBarTracking.setText(" ");
	    }
	    */
	
		@Override
		public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.control_appliances_list_layout, null, true);
		TextView txtTitle = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		
		/*seekBarLabel = (TextView) rowView.findViewById(R.id.seekbarlabel);
		seekBarTracking = (TextView) rowView.findViewById(R.id.seekbartracking);
		
		SeekBar seekBar = (SeekBar) rowView.findViewById(R.id.seekBar1);
		seekBar.setOnSeekBarChangeListener(this);
		seekBar.setMax(100);
	    seekBar.setProgress(50);
		
		seekBarLabel.setText((seekBar.getProgress() + "/" + seekBar.getMax()));
*/
		txtTitle.setText(web[position]);
		imageView.setImageResource(imageId[position]);
		//seekBar.setProgress(seekBarControl[position]);
		
		
		return rowView;
		}
		
}