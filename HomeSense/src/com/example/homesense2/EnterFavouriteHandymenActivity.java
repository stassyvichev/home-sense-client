package com.example.homesense2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class EnterFavouriteHandymenActivity extends Activity{
	
	
	private EditText Name;
	private EditText Occupation;
	private EditText Mobile;
	private FavouriteHandymenDAO favouriteHandymenDAO;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	setContentView(R.layout.enter_handymen);

	favouriteHandymenDAO = new FileDAO(this);
	
	Name = (EditText) findViewById(R.id.Name);
	Occupation= (EditText) findViewById(R.id.Occupation);
	Mobile = (EditText) findViewById(R.id.Mobile);
	
	Button save = (Button) findViewById(R.id.save);
	
	save.setOnClickListener(saveButtonListener); 
}
	
private OnClickListener saveButtonListener = new OnClickListener() {
    	
    	
        public void onClick(View v) {
            if ((Name.getText().length() > 0) && (Occupation.getText().length() > 0) && (Mobile.getText().length() == 11)) {
                FavouriteHandymenDatabase favouritehandymen = new FavouriteHandymenDatabase(Name.getText().toString(), Occupation.getText().toString(), Mobile.getText().toString());
                favouriteHandymenDAO.add(favouritehandymen);
                Name.setText("");
                Occupation.setText("");
                Mobile.setText(""); 
                
               startActivity(new Intent(EnterFavouriteHandymenActivity.this, MainActivity.class));
        		
            }
        }
    };
}

