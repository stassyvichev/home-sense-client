package com.example.homesense2;

import java.util.Locale;

import android.app.ActionBar;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;


public class NotificationsActivity extends FragmentActivity implements ActionBar.TabListener {
	
    SectionsPagerAdapter mSectionsPagerAdapter2;
    ViewPager mViewPager2;

	
	@Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.notifications_main);
	    
	 // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter2 = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager2 = (ViewPager) findViewById(R.id.pager2);
        mViewPager2.setAdapter(mSectionsPagerAdapter2);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager2.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter2.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter2.getPageTitle(i))
                            .setTabListener(this));
        }
  
	    
	  
	  }
	
	 @Override
	public void onPause() {
	     super.onPause();
	     overridePendingTransition(0, 0);
	 }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

    	 MenuInflater inflater = getMenuInflater();
    	    inflater.inflate(R.menu.main, menu);
    	    setTitle("HomeSense - Alerts & Tips");
   	 	    return true;
        }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

    	 switch (item.getItemId()) {
         case R.id.alerts:
        	 super.onBackPressed();
             return true;
         default:
             return super.onOptionsItemSelected(item);
     }
    }
    
    
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager2.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
        	Context context=getApplicationContext();
        	Fragment fragment = new AlertsFragment();
        	
        	if (position == 0){
	            fragment = new AlertsFragment();
	            Bundle args = new Bundle();
	            fragment.setArguments(args);
        	}
        	else if (position == 1){
        		 fragment = new InferencesFragment();
                 Bundle args = new Bundle();
                 fragment.setArguments(args);
        	}
        	
			return fragment;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_notifications_alerts).toUpperCase(l);
                case 1:
                    return getString(R.string.title_notifications_inferences).toUpperCase(l);
                }
            return null;
        }
    }


    public static class AlertsFragment extends Fragment {
        
        public AlertsFragment() {
			super();
		}

		@Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
			
            View rootView = inflater.inflate(R.layout.alerts, container, false);
                                 
            //Values to add to alerts List
            SharedPreferences alertsList= this.getActivity().getSharedPreferences("alerts_list", Context.MODE_PRIVATE);
            Object[] vals=alertsList.getAll().values().toArray();
            String[] values=null;
            if(vals.length!=0){
            	System.out.println(vals.length);
            	values= new String[vals.length];
            	for(int i=0; i<vals.length; i++){
            		values[i]=(String) vals[i];
            		values[i]=values[i].replaceAll("&", "\n");
            		System.out.println(values[i]);
            	}
            }else{
            	values = new String[] { "Alert 1", "Alert 2", "Alert 3", "Alert 4"};  
            }
//            String[] values = new String[] { "Alert 1", "Alert 2", "Alert 3", "Alert 4"};
            // use your own layout
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.alerts_list_layout, R.id.label, values);
            
            ListView alertsListView  = (ListView) rootView.findViewById(R.id.alertslist);
            alertsListView.setAdapter(adapter);
  
            return rootView;
        }
        
        @Override
		public void onResume() {
            super.onResume();
        }
        
    }
    
    
    public static class InferencesFragment extends Fragment {
    	
    	
         	
       @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.inferences, container, false);
            
          //Values to add to alerts List
            String[] tipsStringArray = new String[] { "Save £30 this month by running appliances at night", "Temperature is 21C outside - Turn heating off?", "Electricity Usage is above UK average - Buy energy saving light bulbs?"};
            String[] timeDateStringArray = new String[] { "12th January - 12:32", "6th January - 14:58", "1st January - 17:19"};

            TipsListAdaptor tipsListAdapter = new TipsListAdaptor(getActivity(), tipsStringArray, timeDateStringArray);
            ListView tipsList = (ListView)rootView.findViewById(R.id.tipslist);
            tipsList.setAdapter(tipsListAdapter);
            

                                           
            return rootView;
        }     
  
    }

	
}
